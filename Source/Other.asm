
dialogFlags equ 0xFFFFE8D4
CurTurnNum equ 0xFFFFAEB2
FighingUnitIndex equ 0xFFFFE3E6
WhoAttack equ 0xFFFFE3E2 
AttackedUnitIndex equ 0xFFFFE42E
WhoAttacked equ 0xFFFFE42A
UnitsAddr equ 0x00FF517C
EnemyArmyCnt equ 0x00FF316A

 stru_30CF6:  
	dc.b 0
	dc.b $13
	dc.b   0
	dc.b $10
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_30D04
	dc.b $FF
	dc.b $FF
 byte_30D04:
	dc.b 0                  ; DATA XREF: sub_23974+D38Ao
	dc.b   1
	dc.b   0
	dc.b   1
	dc.b $80 ; А
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   8
	dc.b   0
	dc.b   8
	dc.b $FA ; ·
	dc.b $6E ; n
	dc.b $FE ; ¦
	dc.b   6
	dc.b $71 ; q
	dc.b $74 ; t
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $70 ; p
	dc.b $FE ; ¦
	dc.b   6
	dc.b $73 ; s
	dc.b $76 ; v
	dc.b $FF
 unk_30D3C:
	dc.b   0
	dc.b  $C
	dc.b   0
	dc.b $10
	dc.b $C0 ; L
	dc.b   0
	dc.b $FF
	dc.b $FF

UnitPanel:	
	dc.b 0                  ; DATA XREF: ROM:000101A6o
	dc.b  $C                ; Width
	dc.b   0
	dc.b  $C                ; Heigth
	dc.b $C0                ; Pos
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_30D60
	dc.b $FF
	dc.b $FF
	
stru_30D52:     
	dc.b   0                ; DATA XREF: ROM:0001015Co
	; ROM:000101F0o
	dc.b  $B
	dc.b   0
	dc.b   6
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_30DB0
	dc.b $FF
	dc.b $FF
	
byte_30D60:
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $85 ; Е
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   7
	dc.b   0
	dc.b  $A
	dc.b $FA ; ·
	dc.b $46 ; F
	dc.b $56 ; V
	dc.b $2A ; *
	dc.b $3A ; :
	dc.b $46 ; F
	dc.b $56 ; V
	dc.b $4D ; M
	dc.b $5D ; ]
	dc.b $2C ; ,
	dc.b $3C ; <
	dc.b  $E
	dc.b $1E
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b   0
	dc.b $10
	dc.b $21 ; !
	dc.b $31 ; 1
	dc.b  $E
	dc.b $1E
	dc.b $25 ; %
	dc.b $35 ; 5
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b   6
	dc.b $16
	dc.b   4
	dc.b $14
	dc.b  $C
	dc.b $1C
	dc.b   4
	dc.b $14
	dc.b   0
	dc.b $10
	dc.b   8
	dc.b $18
	dc.b   0
	dc.b $10
	dc.b  $C
	dc.b $1C
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   2
	dc.b $12
	dc.b   2
	dc.b $12
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b   0
	dc.b $10
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b  $A
	dc.b $1A
	dc.b $FE ; ¦
	dc.b   4
	dc.b $65 ; e
	dc.b  $D
	dc.b $1D
	dc.b $FE ; ¦
	dc.b   8
	dc.b $65 ; e
	dc.b   3
	dc.b $13
	dc.b $FF

byte_30DB0:
	dc.b 0                  ; DATA XREF: sub_23974+D3E6o
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $85 ; Е
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   6
	dc.b   0
	dc.b   4
	dc.b $FA ; ·
	dc.b $46 ; F
	dc.b $56 ; V
	dc.b $2A ; *
	dc.b $3A ; :
	dc.b  $E
	dc.b $1E
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b $25 ; %
	dc.b $35 ; 5
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b   4
	dc.b $14
	dc.b   0
	dc.b $10
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   2
	dc.b $12
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b  $A
	dc.b $1A
	dc.b $FF


stru_30DD8:
	dc.b   0                ; DATA XREF: ROM:loc_102CEo
	dc.b $10
	dc.b   0
	dc.b   3
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_30DE6
	dc.b $FF
	dc.b $FF
byte_30DE6:
	dc.b 0                  ; DATA XREF: sub_23974+D46Co
	dc.b   2
	dc.b   0
	dc.b   1
aEnemySUnit:
	dc.b 'Enemy`s unit',$D,$D
	dc.b $FF
	dc.b   0
	
stru_30DFA:     
	dc.b   0                ; DATA XREF: ROM:loc_1028Co
	dc.b $12
	dc.b   0
	dc.b   3
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_30E08
	dc.b $FF
	dc.b $FF
byte_30E08:
	dc.b 0                  ; DATA XREF: sub_23974+D48Eo
	dc.b   2
	dc.b   0
	dc.b   1
aImmovableUnit:
	dc.b 'Immovable unit',$D,$D
	dc.b $FF
	dc.b   0
		
stru_30E1E:     
	dc.b   0                ; DATA XREF: ROM:000107AEo
	dc.b $11
	dc.b   0
	dc.b   3
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_30E2C
	dc.b $FF
	dc.b $FF
byte_30E2C:     
	dc.b 0                  ; DATA XREF: sub_23974+D4B2o
	dc.b   2
	dc.b   0
	dc.b   1
aNotEnoughMp:   
	dc.b 'Not enough MP',$D,$D
	dc.b $FF
	 
stru_30E40:     
	dc.b   0                ; DATA XREF: ROM:000105ACo
	dc.b $14
	dc.b 0
	dc.b   3
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_30E4E
	dc.b $FF
	dc.b $FF
byte_30E4E:     
	dc.b 0                  ; DATA XREF: sub_23974+D4D4o
	dc.b   2
	dc.b   0
	dc.b   1
aCannotUseMagic:
	dc.b 'Cannot use magic',$D,$D
	dc.b $FF
	dc.b   0
	
byte_30E66:    
    dc.b   0                 
	dc.b   2
	dc.b   0
	dc.b   4
	dc.b   0
	dc.b   6
	dc.b   0
	dc.b   8
	dc.b   0
	dc.b  $A
	dc.b $FF
    dc.b $FF
	
byte_30E72:     
    dc.b   0               
	dc.b   2
	dc.b   0
	dc.b   4
	dc.b $FF
	dc.b $FF
	dc.b   0
	dc.b $18
	dc.b   0
	dc.b $16
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_30E86
	dc.b $FF
	dc.b $FF
byte_30E86:     
	dc.b 0                  ; DATA XREF: sub_23974+D50Co
	dc.b   2
	dc.b   0
	dc.b   2
	dc.b $20
	
aUnitXYHpEnd:   
    dc.b 'Unit   X  Y  HP END',$D,$D
	dc.b $FF
	dc.b   0
	dc.b   0
	dc.b  $D
	dc.b   0
	dc.b $15
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_30EB0
	dc.b $FF
	dc.b $FF
byte_30EB0:     
	dc.b 0                  ; DATA XREF: sub_23974+D536o
	dc.b   2
	dc.b   0
	dc.b   2
	
aUnitHp:        
    dc.b ' Unit   HP',$D,$D
	dc.b $FF
	dc.b   0

byte_30EC2:     
    dc.b $2D,$2D,$FF,  0,  0,  4,  0,  6,  0,  8,  0, $A,  0, $C,  0, $E
	dc.b   0,$10,  0,$12,$FF,$FF
	
stru_30ED8:    
	dc.b   0                ; DATA XREF: ROM:0001038Ao
	dc.b  $E
	dc.b   0
	dc.b  $A
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_30EE6
	dc.b $FF
	dc.b $FF
byte_30EE6:
	dc.b 0                  ; DATA XREF: sub_23974+D56Co
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $85 ; Е
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   7
	dc.b   0
	dc.b   8
	dc.b $FA ; ·
	dc.b $46 ; F
	dc.b $56 ; V
	dc.b $2D ; -
	dc.b $3D ; =
	dc.b $2A ; *
	dc.b $3A ; :
	dc.b $46 ; F
	dc.b $56 ; V
	dc.b  $E
	dc.b $1E
	dc.b   4
	dc.b $14
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b   0
	dc.b $10
	dc.b $25 ; %
	dc.b $35 ; 5
	dc.b   5
	dc.b $15
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b  $D
	dc.b $1D
	dc.b   4
	dc.b $14
	dc.b   4
	dc.b $14
	dc.b   0
	dc.b $10
	dc.b $24 ; $
	dc.b $34 ; 4
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b  $D
	dc.b $1D
	dc.b   2
	dc.b $12
	dc.b   0
	dc.b $10
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   2
	dc.b $12
	dc.b  $A
	dc.b $1A
	dc.b  $B
	dc.b $1B
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   4
	dc.b $14
	dc.b $FE ; ¦
	dc.b   4
	dc.b $65 ; e
	dc.b $FF
	dc.b   0

stru_30F2E:     
	dc.b   0                ; DATA XREF: ROM:00010488o
	dc.b  $B
	dc.b   0
	dc.b   8
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_30F3C
	dc.b $FF
	dc.b $FF
byte_30F3C:
	dc.b 0                  ; DATA XREF: sub_23974+D5C2o
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $85 ; Е
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   7
	dc.b   0
	dc.b   6
	dc.b $FA ; ·
	dc.b $40 ; @
	dc.b $50 ; P
	dc.b $4B ; K
	dc.b $5B ; [
	dc.b $44 ; D
	dc.b $54 ; T
	dc.b $41 ; A
	dc.b $51 ; Q
	dc.b $4C ; L
	dc.b $5C ; \
	dc.b $45 ; E
	dc.b $55 ; U
	dc.b $62 ; b
	dc.b $72 ; r
	dc.b $4D ; M
	dc.b $5D ; ]
	dc.b $46 ; F
	dc.b $56 ; V
	dc.b $42 ; B
	dc.b $52 ; R
	dc.b $4E ; N
	dc.b $5E ; ^
	dc.b $47 ; G
	dc.b $57 ; W
	dc.b $43 ; C
	dc.b $53 ; S
	dc.b $2D ; -
	dc.b $2D ; -
	dc.b $48 ; H
	dc.b $58 ; X
	dc.b $FE ; ¦
	dc.b   4
	dc.b $2D ; -
	dc.b $49 ; I
	dc.b $59 ; Y
	dc.b $FE ; ¦
	dc.b   4
	dc.b $2D ; -
	dc.b $4A ; J
	dc.b $5A ; Z
	dc.b $FF
	dc.b   0
	dc.b   1
	dc.b   0
	dc.b   3
	dc.b   0
	dc.b   5
	dc.b $FF
	dc.b $FF
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   4
	dc.w 6
	dc.b $FF
	dc.b $FF
	
word_30F84:    
	dc.w 2             
	dc.w 4
	dc.w 6
	dc.w 8
	dc.b $FF
	dc.b $FF
	
word_30F8E:     
	dc.w 2                 
	dc.w 4
	dc.w 6
	dc.b $FF
	dc.b $FF
	
word_30F96:     
	dc.w $15             
	dc.w 9
	dc.w $C000
	dc.b $FF
	dc.b $FF
	
word_30F9E:     
	dc.w 0                  
	dc.w 0

word_30FA2:     ;Положение портретов
	dc.w $FC00             
	dc.w 0
	
aWinLose_0:     
	dc.b 'Win',$D,$D,'Lose'
	dc.b $FF
	
word_30FB0:     
	dc.w $24
	dc.w 7
	dc.w $C000
	dc.b $FF
	dc.b $FF
	
	
stru_30FB8:     
	dc.b   0                ; DATA XREF: ROM:000109AEo
	dc.b $15
	dc.b   0
	dc.b $10
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_30FC6
	dc.b $FF
	dc.b $FF
byte_30FC6:     
	dc.b 0                  ; DATA XREF: sub_23974+D64Co
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $85 ; Е
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b $11
	dc.b   0
	dc.b  $E
	dc.b $FA ; ·
	dc.b $2E ; .
	dc.b $3E ; >
	dc.b $2F ; /
	dc.b $3F ; ?
	dc.b $60 ; `
	dc.b $70 ; p
	dc.b $4C ; L
	dc.b $5C ; \
	dc.b $40 ; @
	dc.b $50 ; P
	dc.b $2B ; +
	dc.b $3B ; ;
	dc.b $42 ; B
	dc.b $52 ; R
	dc.b  $D
	dc.b $1D
	dc.b $24 ; $
	dc.b $34 ; 4
	dc.b   8
	dc.b $18
	dc.b   4
	dc.b $14
	dc.b   0
	dc.b $10
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b  $D
	dc.b $1D
	dc.b   3
	dc.b $13
	dc.b  $B
	dc.b $1B
	dc.b  $D
	dc.b $1D
	dc.b  $C
	dc.b $1C
	dc.b  $C
	dc.b $1C
	dc.b $40 ; @
	dc.b $50 ; P
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b  $B
	dc.b $1B
	dc.b  $D
	dc.b $1D
	dc.b   8
	dc.b $18
	dc.b   4
	dc.b $14
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   4
	dc.b $14
	dc.b  $E
	dc.b $1E
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   8
	dc.b $18
	dc.b $64 ; d
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b $46 ; F
	dc.b $56 ; V
	dc.b $21 ; !
	dc.b $31 ; 1
	dc.b   5
	dc.b $15
	dc.b $4C ; L
	dc.b $5C ; \
	dc.b  $D
	dc.b $1D
	dc.b   0
	dc.b $10
	dc.b $4C ; L
	dc.b $5C ; \
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b $21 ; !
	dc.b $31 ; 1
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   8
	dc.b $18
	dc.b   6
	dc.b $16
	dc.b $24 ; $
	dc.b $34 ; 4
	dc.b  $F
	dc.b $1F
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b $24 ; $
	dc.b $34 ; 4
	dc.b $49 ; I
	dc.b $59 ; Y
	dc.b $29 ; )
	dc.b $39 ; 9
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b   4
	dc.b $14
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b  $F
	dc.b $1F
	dc.b   7
	dc.b $17
	dc.b   4
	dc.b $14
	dc.b $2C ; ,
	dc.b $3C ; <
	dc.b  $E
	dc.b $1E
	dc.b   4
	dc.b $14
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b   0
	dc.b $10
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b  $E
	dc.b $1E
	dc.b  $C
	dc.b $1C
	dc.b   3
	dc.b $13
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   8
	dc.b $18
	dc.b $22 ; "
	dc.b $32 ; 2
	dc.b $46 ; F
	dc.b $56 ; V
	dc.b  $D
	dc.b $1D
	dc.b   0
	dc.b $10
	dc.b $FE ; ¦
	dc.b   4
	dc.b $65 ; e
	dc.b  $E
	dc.b $1E
	dc.b   4
	dc.b $14
	dc.b   0
	dc.b $10
	dc.b   3
	dc.b $13
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b $FE ; ¦
	dc.b   4
	dc.b $65 ; e
	dc.b  $D
	dc.b $1D
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b  $F
	dc.b $1F
	dc.b   8
	dc.b $18
	dc.b   8
	dc.b $18
	dc.b $FE ; ¦
	dc.b  $A
	dc.b $65 ; e
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b   2
	dc.b $12
	dc.b $FE ; ¦
	dc.b  $A
	dc.b $65 ; e
	dc.b   8
	dc.b $18
	dc.b $FE ; ¦
	dc.b  $C
	dc.b $65 ; e
	dc.b  $E
	dc.b $1E
	dc.b $FE ; ¦
	dc.b  $C
	dc.b $65 ; e
	dc.b  $D
	dc.b $1D
	dc.b $FE ; ¦
	dc.b   8
	dc.b $65 ; e
	dc.b $FF
	dc.b   0
stru_3109E:     
	dc.b   0                ; DATA XREF: ROM:00010A78o
	dc.b  $A
	dc.b   0
	dc.b   8
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_310AC
	dc.b $FF
	dc.b $FF
	
 byte_310AC:     
	dc.b 0                  ; DATA XREF: sub_23974+D732o
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $85 ; Е
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   6
	dc.b   0
	dc.b   6
	dc.b $FA ; ·
	dc.b $4C ; L
	dc.b $5C ; \
	dc.b $47 ; G
	dc.b $57 ; W
	dc.b $2F ; /
	dc.b $3F ; ?
	dc.b  $B
	dc.b $1B
	dc.b  $E
	dc.b $1E
	dc.b   0
	dc.b $10
	dc.b  $E
	dc.b $1E
	dc.b $21 ; !
	dc.b $31 ; 1
	dc.b $22 ; "
	dc.b $32 ; 2
	dc.b $26 ; &
	dc.b $36 ; 6
	dc.b  $C
	dc.b $1C
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   0
	dc.b $10
	dc.b $FE ; ¦
	dc.b   4
	dc.b $65 ; e
	dc.b  $B
	dc.b $1B
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b $FF
	dc.b   0
 stru_310E0:      
	dc.b   0                ; DATA XREF: ROM:00010B08o
	; ROM:00010BA8o
	dc.b   7
	dc.b   0
	dc.b   6
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_310EE
	dc.b $FF
	dc.b $FF
 byte_310EE:    
	dc.b 0                  ; DATA XREF: sub_23974+D774o
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $85 ; Е
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b 0
	dc.b   3
	dc.b   0
	dc.b   4
	dc.b $FA ; ·
	dc.b $48 ; H
	dc.b $58 ; X
	dc.b $48 ; H
	dc.b $58 ; X
	dc.b  $D
	dc.b $1D
	dc.b   5
	dc.b $15
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b   5
	dc.b $15
	dc.b $FF
word_3110A:     dc.w     2              ; 0 ; DATA XREF: sub_109D2+Ao
                dc.w 4
                dc.w 6
                dc.w 8
                dc.w $A
                dc.w $C
                dc.w $E
                dc.w $FFFF
unk_3111A:       
	dc.b   0                ; DATA XREF: ROM:00010AA6o
	dc.b   2
	dc.b   0
	dc.b   4
	dc.b   0
	dc.b   6
	dc.b $FF
	dc.b $FF
 unk_31122:      
	dc.b   0                ; DATA XREF: ROM:00010B36o
			 ; ROM:00010BD6o
	dc.b   2
	dc.b   0
	dc.b   4
	dc.b $FF
	dc.b $FF
	
aCancel:        
	dc.b 'Cancel'          
	dc.b $FF
	dc.b   0
	
byte_31130:     
	dc.b 0               
	dc.b   2
	dc.b   0
	dc.b   4
	dc.b 0
	dc.b   6
	dc.b   0
	dc.b   8
	dc.b   0
	dc.b  $A
	dc.b $FF
	dc.b $FF
	
byte_3113C:     
	dc.b   0                 
	dc.b   2
	dc.b   0
	dc.b   4
	dc.b   0
	dc.b   6
	dc.b   0
	dc.b   8
	dc.b $FF
	dc.b $FF
	
byte_31146:     
	dc.b   0              
	dc.b   2
	dc.b   0
	dc.b   4
	dc.b   0
	dc.b   6
	dc.b $FF
	dc.b $FF
	
unk_3114E:      
	dc.b   0               
	dc.b $12
	dc.b   0
	dc.b  $D
	dc.b $C0 ; L
	dc.b   0
	dc.b $FF
	dc.b $FF
	
unk_31156:      
	dc.b   0                
	dc.b $12
	dc.b   0
	dc.b  $B
	dc.b $C0 ; L
	dc.b   0
	dc.b $FF
	dc.b $FF
	
stru_3115E:     
	dc.b   0               
	dc.b $11
	dc.b   0
	dc.b   9
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_3116C
	dc.b $FF
	dc.b $FF
byte_3116C:
	dc.b 0                  ; DATA XREF: sub_23974+D7F2o
	dc.b   2
	dc.b   0
	dc.b   2
	dc.b 'New game',$D,$D,'Load game',$D,$D,'Continue game',$D,$D
	dc.b $FF
	dc.b   0
	
stru_31196:      
	dc.b   0              
	dc.b $13
	dc.b   0
	dc.b   3
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_311A4
	dc.b $FF
	dc.b $FF
byte_311A4:     
	dc.b 0                  ; DATA XREF: sub_23974+D82Ao
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b 'Saving the data'     ;ghjghjhgjhgjghjghj    
	dc.b $FF
	
stru_311B8:      
    dc.b   0               
	dc.b $14
	dc.b   0
	dc.b   3
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_311C6
	dc.b $FF
	dc.b $FF
byte_311C6:     
	dc.b 0                  ; DATA XREF: sub_23974+D84Co
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b 'Loading the data'
	dc.b $FF
	dc.b   0
	dc.b $7E ; ~
	dc.b $BC ; -
	dc.b $B7 ; ¬
	dc.b $B6 ; ¦
	dc.b $DD ; ¦
	dc.b $CA ; ¦
	dc.b $B2 ; -
	dc.b $C1 ; +
	dc.b $FF
	dc.b   0
	
 stru_311e6:     
	dc.b   0                ; DATA XREF: ROM:0001115Ao
	dc.b $13
	dc.b   0
	dc.b $10
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_311F4
	dc.b $FF
	dc.b $FF
 byte_311F4:     
	dc.b 0                  ; DATA XREF: sub_23974+D87Ao
	dc.b   1
	dc.b   0
	dc.b   1
	dc.b $80 ; А
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   8
	dc.b   0
	dc.b   8
	dc.b $FA ; ·
	dc.b $6E ; n
	dc.b $FE ; ¦
	dc.b   6
	dc.b $71 ; q
	dc.b $74 ; t
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $70 ; p
	dc.b $FE ; ¦
	dc.b   6
	dc.b $73 ; s
	dc.b $76 ; v
	dc.b $FF
	dc.b   0
	dc.b   8
	dc.b   0
	dc.b $19
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_3123A
	dc.b $FF
	dc.b $FF
 byte_3123A:     
	dc.b 0                  ; DATA XREF: sub_23974+D8C0o
	dc.b   2
	dc.b   0
	dc.b   2
	dc.b $58 ; X
	dc.b $20
	dc.b $20
	dc.b $59 ; Y
	dc.b  $D
	dc.b  $D
	dc.b $FF
	dc.b   0
	dc.b   0
	dc.b  $D
	dc.b   0
	dc.b $16
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_31254
	dc.b $FF
	dc.b $FF
 byte_31254:  
	dc.b 0                  ; DATA XREF: sub_23974+D8DAo
	dc.b   2
	dc.b   0
	dc.b   2
	dc.b $BC ; -
	dc.b $B7 ; ¬
	dc.b $B6 ; ¦
	dc.b $DD ; ¦
	dc.b  $D
	dc.b  $D
	dc.b $FF
	dc.b   0
	
selmenuicosz?:      
	dc.b $FE
	dc.b   5
	dc.b $FF
	dc.b   0
	
byte_31264:     
	dc.b 0                  
	dc.b   4
	dc.b   0
	dc.b   6
	dc.b   0
	dc.b   8
	dc.b   0
	dc.b  $A
	dc.b   0
	dc.b  $C
	dc.b   0
	dc.b  $E
	dc.b   0
	dc.b $10
	dc.b   0
	dc.b $12
	dc.b   0
	dc.b $14
	dc.b   0
	dc.b $16
	dc.b $FF
	dc.b $FF

	
Start_pos_cnt:
	incbin ScenarioData\Scenario_1\StartPositionCnt
	incbin ScenarioData\Scenario_2\StartPositionCnt
	incbin ScenarioData\Scenario_3\StartPositionCnt
	incbin ScenarioData\Scenario_4\StartPositionCnt
	incbin ScenarioData\Scenario_5\StartPositionCnt
	incbin ScenarioData\Scenario_6\StartPositionCnt
	incbin ScenarioData\Scenario_7\StartPositionCnt
	incbin ScenarioData\Scenario_8\StartPositionCnt
	incbin ScenarioData\Scenario_9\StartPositionCnt
	incbin ScenarioData\Scenario_10\StartPositionCnt
	incbin ScenarioData\Scenario_11\StartPositionCnt
	incbin ScenarioData\Scenario_12\StartPositionCnt
	incbin ScenarioData\Scenario_13\StartPositionCnt
	incbin ScenarioData\Scenario_14\StartPositionCnt
	incbin ScenarioData\Scenario_15\StartPositionCnt
	incbin ScenarioData\Scenario_16\StartPositionCnt
	incbin ScenarioData\Scenario_17\StartPositionCnt
	incbin ScenarioData\Scenario_18\StartPositionCnt
	incbin ScenarioData\Scenario_19\StartPositionCnt
	incbin ScenarioData\Scenario_20\StartPositionCnt

Start_positions:
	dc.l Start_pos_l1       ; DATA XREF: sub_1101E+2Eo
	dc.l Start_pos_l2
	dc.l Start_pos_l3
	dc.l Start_pos_l4
	dc.l Start_pos_l5
	dc.l Start_pos_l6
	dc.l Start_pos_l7
	dc.l Start_pos_l8
	dc.l Start_pos_l9
	dc.l Start_pos_l10
	dc.l Start_pos_l11
	dc.l Start_pos_l12
	dc.l Start_pos_l13
	dc.l Start_pos_l14
	dc.l Start_pos_l15
	dc.l Start_pos_l16
	dc.l Start_pos_l17
	dc.l Start_pos_l18
	dc.l Start_pos_l19
	dc.l Start_pos_l20
Start_pos_l1:
	incbin ScenarioData\Scenario_1\StartPositions
Start_pos_l2:
	incbin ScenarioData\Scenario_2\StartPositions
Start_pos_l3:
	incbin ScenarioData\Scenario_3\StartPositions
Start_pos_l4:
	incbin ScenarioData\Scenario_4\StartPositions
Start_pos_l5:
	incbin ScenarioData\Scenario_5\StartPositions
Start_pos_l6:
	incbin ScenarioData\Scenario_6\StartPositions
Start_pos_l7:
	incbin ScenarioData\Scenario_7\StartPositions
Start_pos_l8:
	incbin ScenarioData\Scenario_8\StartPositions
Start_pos_l9:
	incbin ScenarioData\Scenario_9\StartPositions
Start_pos_l10:
	incbin ScenarioData\Scenario_10\StartPositions
Start_pos_l11:
	incbin ScenarioData\Scenario_11\StartPositions
Start_pos_l12:
	incbin ScenarioData\Scenario_12\StartPositions
Start_pos_l13:
	incbin ScenarioData\Scenario_13\StartPositions
Start_pos_l14:
	incbin ScenarioData\Scenario_14\StartPositions
Start_pos_l15:
	incbin ScenarioData\Scenario_15\StartPositions
Start_pos_l16:
	incbin ScenarioData\Scenario_16\StartPositions
Start_pos_l17:
	incbin ScenarioData\Scenario_17\StartPositions
Start_pos_l18:
	incbin ScenarioData\Scenario_18\StartPositions
Start_pos_l19:
	incbin ScenarioData\Scenario_19\StartPositions
Start_pos_l20:
	incbin ScenarioData\Scenario_20\StartPositions
	
 whowherevdp?:     
	dc.w 2                                
	dc.w 8
	dc.w 2
	dc.w $FA00
	dc.w $801
	dc.w $902
	dc.w $A03
	dc.w $B04
	dc.w $C05
	dc.w $D06
	dc.w $E07
	dc.w $FFF
	
 cursorparamonsel:
    dc.w $FA0A 
    dc.w $FA00
	
 cursorposonherosel:
    dc.w     0,    3,    6,    9,   $C,   $F,  $12,  $15,$FFFF	 
	
 stru_31504:    
	dc.b   0                ; DATA XREF: ROM:00011B24o
	dc.b  $B
	dc.b   0
	dc.b   8
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_31512
	dc.b $FF
	dc.b $FF
 byte_31512:     
	dc.b 0                  ; DATA XREF: sub_23974+DB98o
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $85 ; Е
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   7
	dc.b   0
	dc.b   6
	dc.b $FA ; ·
	dc.b $4C ; L
	dc.b $5C ; \
	dc.b $42 ; B
	dc.b $52 ; R
	dc.b $4C ; L
	dc.b $5C ; \
	dc.b  $E
	dc.b $1E
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b  $B
	dc.b $1B
	dc.b   4
	dc.b $14
	dc.b   0
	dc.b $10
	dc.b   3
	dc.b $13
	dc.b  $C
	dc.b $1C
	dc.b $21 ; !
	dc.b $31 ; 1
	dc.b   8
	dc.b $18
	dc.b $65 ; e
	dc.b $65 ; e
	dc.b $23 ; #
	dc.b $33 ; 3
	dc.b   4
	dc.b $14
	dc.b $FE ; ¦
	dc.b   4
	dc.b $65 ; e
	dc.b $21 ; !
	dc.b $31 ; 1
	dc.b $FE ; ¦
	dc.b   4
	dc.b $65 ; e
	dc.b $FF
	
	; dc.b   1
	; dc.b   1
	
 unk_3154A:      
    dc.b   0 
	dc.b   2
	dc.b   0
	dc.b   4
	dc.b   0
	dc.b   6
	dc.b $FF
	dc.b $FF
	

stru_31552: 
	dc.b   0                ; DATA XREF: ROM:00011F70o
	dc.b $13
	dc.b   0
	dc.b   4
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_31560
	dc.b $FF
	dc.b $FF
byte_31560:
	dc.b 0                  ; DATA XREF: sub_23974+DBE6o
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $81 ; Б
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b  $F
	dc.b   0
	dc.b   2
	dc.b $FA ; ·
	dc.b $AA ; к
	dc.b $B1 ; -
	dc.b $AB ; л
	dc.b $B2 ; -
	dc.b $AC ; м
	dc.b $B3 ; ¦
	dc.b $AD ; н
	dc.b $B4 ; +
	dc.b $AE ; о
	dc.b $B5 ; ¦
	dc.b $AF ; п
	dc.b $B6 ; ¦
	dc.b $B0 ; -
	dc.b $B7 ; ¬
	dc.b $B8 ; ¬
	dc.b $B9 ; ¦
	dc.b $BA ; ¦
	dc.b $BB ; ¬
	dc.b   3
	dc.b   3
	dc.b $8A ; К
	dc.b $9A ; Ъ
	dc.b $8B ; Л
	dc.b $9B ; Ы
	dc.b $8C ; М
	dc.b $9C ; Ь
	dc.b $8D ; Н
	dc.b $9D ; Э
	dc.b $8E ; О
	dc.b $9E ; Ю
	dc.b $FF

	
 stru_3158E:      
    dc.b   0             
	dc.b $13
	dc.b 0
	dc.b   3
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.b   0
	dc.b   3
	dc.b $15
	dc.b $9C ; Ь
	dc.b $FF
	dc.b $FF
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b 'Cannot use item',$D,$D
	dc.b $FF

 aItem:          
    dc.b 'ITEM'            
	dc.b $FF
	dc.b   0

stru_315B8:
	dc.b   0                ; DATA XREF: ROM:00012246o
	dc.b $14
	dc.b   0
	dc.b   4
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_315C6
	dc.b $FF
	dc.b $FF
byte_315C6:
	dc.b 0                  ; DATA XREF: sub_23974+DC4Co
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $81 ; Б
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b $10
	dc.b   0
	dc.b   2
	dc.b $FA ; ·
	dc.b $AA ; к
	dc.b $B1 ; -
	dc.b $AB ; л
	dc.b $B2 ; -
	dc.b $AC ; м
	dc.b $B3 ; ¦
	dc.b $AD ; н
	dc.b $B4 ; +
	dc.b $AE ; о
	dc.b $B5 ; ¦
	dc.b $AF ; п
	dc.b $B6 ; ¦
	dc.b $B0 ; -
	dc.b $B7 ; ¬
	dc.b $B8 ; ¬
	dc.b $B9 ; ¦
	dc.b   3
	dc.b   3
	dc.b $35 ; 5
	dc.b $3C ; <
	dc.b $36 ; 6
	dc.b $3D ; =
	dc.b $37 ; 7
	dc.b $3E ; >
	dc.b $38 ; 8
	dc.b $3F ; ?
	dc.b $39 ; 9
	dc.b $40 ; @
	dc.b $3A ; :
	dc.b $41 ; A
	dc.b $3B ; ;
	dc.b $42 ; B
	dc.b $FF

 unk_315F6:      
	dc.b $20         
	dc.b $20
	dc.b $20
	dc.b $FF
	
 unk_315FA:     
	dc.b   0               
	dc.b $12
	dc.b   0
	dc.b   4
	dc.b $C0 ; L
	dc.b   0
	dc.b $FF
	dc.b $FF
	
 unk_31602:      
    dc.b $20              
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $FF
	dc.b   0

 word_3160E:     
    dc.w     2,    2,   $B,    2,  $14,    2,  $1D,    2,    2,  $16,   $B,  $16,  $14,  $16,  $1D,  $16

     
 byte_3162E:
	dc.b   0                ; DATA XREF: ROM:000133B6o
	dc.b $14
	dc.b   0
	dc.b   4
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   5
	dc.l byte_3163C
	dc.b $FF
	dc.b $FF
 byte_3163C:
	dc.b 0                  ; DATA XREF: sub_23974+DCC2o
	dc.b   2
	dc.b   0
	dc.b   1
	dc.b $81 ; Б
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b $10
	dc.b   0
	dc.b   2
	dc.b $FA ; ·
	dc.b $AA ; к
	dc.b $B1 ; -
	dc.b $AB ; л
	dc.b $B2 ; -
	dc.b $AC ; м
	dc.b $B3 ; ¦
	dc.b $AD ; н
	dc.b $B4 ; +
	dc.b $AE ; о
	dc.b $B5 ; ¦
	dc.b $AF ; п
	dc.b $B6 ; ¦
	dc.b $B0 ; -
	dc.b $B7 ; ¬
	dc.b $B8 ; ¬
	dc.b $B9 ; ¦
	dc.b   3
	dc.b   3
	dc.b $6E ; n
	dc.b $7E ; ~
	dc.b $6F ; o
	dc.b $7F ; 
	dc.b $80 ; А
	dc.b $90 ; Р
	dc.b $81 ; Б
	dc.b $91 ; С
	dc.b $82 ; В
	dc.b $92 ; Т
	dc.b $83 ; Г
	dc.b $93 ; У
	dc.b $84 ; Д
	dc.b $94 ; Ф
	dc.b $FF
	
 word_3166C:     
    dc.w     2,    9,    2,   $B,    2,   $D,    2,   $F,    2,  $11,    2,  $13,   $B,    9,   $B,   $B
	dc.w    $B,   $D,   $B,   $F,   $B,  $11,   $B,  $13
	
 unk_3169C:      
	dc.b $20                
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $20
	dc.b $FF
	dc.b   0
	
 aEnd:           
    dc.b 'END'              
    dc.b $FF
	
 word_316AA:     
    dc.w 0            
	dc.w $468
	dc.w $446
	dc.w $244
	dc.w $222
	dc.w $224
	dc.w $AE
	dc.w $48
	dc.w $280
	dc.w $40
	dc.w $E
	dc.w $E0E
	dc.w $E0E
	dc.w $400
	dc.w 0
	dc.w $EEE
	
	; dc.w $000   ;for lang 2 mod
	; dc.w $888
	; dc.w $222
	; dc.w $EEE
	; dc.w $E64
	; dc.w $C00
	; dc.w $6AC
	; dc.w $248
	; dc.w $2C2
	; dc.w $062
	; dc.w $0AE
	; dc.w $00C
	; dc.w $006
	; dc.w $E0E
	; dc.w $644
	; dc.w $EC6
	
 byte_316CA:
    dc.b   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0; 0
	dc.b   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0; 16
	
 word_316EA:
    dc.w   $10,   $B,  $10,   $D,  $10,   $F,  $10,  $11,  $10,  $13,  $1B,   $B,  $1B,   $D,  $1B,   $F; 0
	dc.w   $1B,  $11,  $1B,  $13; 16

 asc_31712:      
    dc.b '            ',$D,'            '
	dc.b $FF
		
	
 byte_3172C:     
    dc.b   0,  1,  0,$28,  0, $E,$FA,  3,$15,$FE,$12,$16,$17,$A2,$FE,  6
	dc.b $A3,$A4,$A1,$FE,  8,$16,$17,  3,  3,$18,$FE,$12,  3,$19,$A5,$FE
	dc.b   6,  3,$A6,$FE,  9,  3,$19,  3,  3,$18,$FE,$12,  3,$19,$A5,$FE
	dc.b   6,  3,$A6,$FE,  9,  3,$19,  3,  3,$18,$FE,$12,  3,$19,$A5,$FE
	dc.b   6,  3,$A6,$FE,  9,  3,$19,  3,  3,$18,$FE,$12,  3,$19,$A5,$FE
	dc.b   6,  3,$A6,$FE,  9,  3,$19,  3,  3,$18,$FE,$12,  3,$19,$A5,$FE
	dc.b   6,  3,$A6,$FE,  9,  3,$19,  3,  3,$18,$FE,$12,  3,$19,$A5,$FE
	dc.b   6,  3,$A6,$FE,  9,  3,$19,  3,  3,$18,$FE,$12,  3,$19,$A7,$FE
	dc.b   6,$A8,$A9,$FE,  9,  3,$19,  3,  3,$18,$FE,$12,  3,$19,$A0,$FE
	dc.b $10,  3,$19,  3,  3,$18,$FE,$12,  3,$19,$18,$FE,$10,  3,$19,  3
	dc.b   3,$18,$FE,$12,  3,$19,$18,$FE,$10,  3,$19,  3,  3,$18,$FE,$12
	dc.b   3,$19,$18,$FE,$10,  3,$19,  3,  3,$18,$FE,$12,  3,$19,$18,$FE
	dc.b $10,  3,$19,  3,  3,$1A,$FE,$12,$1B,$1C,$1A,$FE,$10,$1B,$1C,  3
	dc.b $FF,  0

 byte_317FE:     
	dc.b   0,  1,  0,$28,  0, $E,$FA,  3,  3,$A2,$FE,  6,$A3,$A4,$A1,$FE
	dc.b   8,$16,$17,$15,$FE,$10,$16,$17,$FE,  4,  3,$A5,$FE,  6,  3,$A6
	dc.b $FE,  9,  3,$19,$18,$FE,$10,  3,$19,$FE,  4,  3,$A5,$FE,  6,  3
	dc.b $A6,$FE,  9,  3,$19,$18,$FE,$10,  3,$19,$FE,  4,  3,$A5,$FE,  6
	dc.b   3,$A6,$FE,  9,  3,$19,$18,$FE,$10,  3,$19,$FE,  4,  3,$A5,$FE
	dc.b   6,  3,$A6,$FE,  9,  3,$19,$18,$FE,$10,  3,$19,$FE,  4,  3,$A5
	dc.b $FE,  6,  3,$A6,$FE,  9,  3,$19,$18,$FE,$10,  3,$19,$FE,  4,  3
	dc.b $A5,$FE,  6,  3,$A6,$FE,  9,  3,$19,$18,$FE,$10,  3,$19,$FE,  4
	dc.b   3,$A7,$FE,  6,$A8,$A9,$FE,  9,  3,$19,$18,$FE,$10,  3,$19,$FE
	dc.b   4,  3,$A0,$FE,$10,  3,$19,$18,$FE,$10,  3,$19,$FE,  4,  3,$18
	dc.b $FE,$10,  3,$19,$18,$FE,$10,  3,$19,$FE,  4,  3,$18,$FE,$10,  3
	dc.b $19,$18,$FE,$10,  3,$19,$FE,  4,  3,$18,$FE,$10,  3,$19,$18,$FE
	dc.b   8,  3,$13,$FE,  7,  3,$19,$FE,  4,  3,$18,$FE,$10,  3,$19,$18
	dc.b $FE,$10,  3,$19,$FE,  4,  3,$1A,$FE,$10,$1B,$1C,$1A,$FE,$10,$1B
	dc.b $1C,  3,  3,$FF
	
 byte_318E2:     
	dc.b   0,  1,  0,$28,  0, $E,$FA,  3,  3,$A2,$FE,  6,$A3,$A4,$A1,$16
	dc.b $16,$16,$17,$15,$FE,$16,$16,$17,  3,  3,  3,$A5,$FE,  6,  3,$A6
	dc.b $FE,  4,  3,$19,$18,$FE,$16,  3,$19,  3,  3,  3,$A5,$FE,  6,  3
	dc.b $A6,$FE,  4,  3,$19,$18,$FE,$16,  3,$19,  3,  3,  3,$A5,$FE,  6
	dc.b   3,$A6,$FE,  4,  3,$19,$18,$FE,$16,  3,$19,  3,  3,  3,$A5,$FE
	dc.b   6,  3,$A6,$FE,  4,  3,$19,$18,$FE,$16,  3,$19,  3,  3,  3,$A5
	dc.b $FE,  6,  3,$A6,$FE,  4,  3,$19,$18,$FE,$16,  3,$19,  3,  3,  3
	dc.b $A5,$FE,  6,  3,$A6,$FE,  4,  3,$19,$18,$FE,$16,  3,$19,  3,  3
	dc.b   3,$A7,$FE,  6,$A8,$A9,$FE,  4,  3,$19,$18,$FE,$16,  3,$19,  3
	dc.b   3,  3,$A0,$FE, $B,  3,$19,$18,$FE,$16,  3,$19,  3,  3,  3,$18
	dc.b $FE, $B,  3,$19,$18,$FE,$16,  3,$19,  3,  3,  3,$18,$FE, $B,  3
	dc.b $19,$18,$FE,$16,  3,$19,  3,  3,  3,$18,$FE, $B,  3,$19,$18,$FE
	dc.b $16,  3,$19,  3,  3,  3,$18,$FE, $B,  3,$19,$18,$FE,$16,  3,$19
	dc.b   3,  3,  3,$1A,$FE, $B,$1B,$1C,$1A,$FE,$16,$1B,$1C,  3,$FF,  0
	
 byte_319C2:     
	dc.b   0,  1,  0,$28,  0,$1C,$FA,$FE,$2A,  2,$FE,  8,  5,  2,$FE,  8
	dc.b   5,  2,$FE,  8,  5,  2,$FE,  8,  5,$FE,  4,  2,  7,$FE,  8,  3
	dc.b   9,$FE,  8,  3,  9,$FE,  8,  3,  9,$FE,  8,  3,  6,  2,  2,  2
	dc.b   7,$FE,  8,  3,  9,$FE,  8,  3,  9,$FE,  8,  3,  9,$FE,  8,  3
	dc.b   6,  2,  2,  2,  7,$FE,  8,  3,  9,$FE,  8,  3,  9,$FE,  8,  3
	dc.b   9,$FE,  8,  3,  6,  2,  2,  2,  7,$FE,  8,  3,  9,$FE,  8,  3
	dc.b   9,$FE,  8,  3,  9,$FE,  8,  3,  6,  2,  2,  5,  5,$FE,  8,  8
	dc.b   5,$FE,  8,  8,  5,$FE,  8,  8,  5,$FE,  8,  8,  5,  5,  5,$FE
	dc.b $FF,  3,$FE,$FF,  3,$FE,$32,  3,  4,  4,$FE,  8,  8,  4,$FE,  8
	dc.b   8,  4,$FE,  8,  8,  4,$FE,  8,  8,  4,  4,  4,  2,  7,$FE,  8
	dc.b   3,  9,$FE,  8,  3,  9,$FE,  8,  3,  9,$FE,  8,  3,  6,  2,  2
	dc.b   2,  7,$FE,  8,  3,  9,$FE,  8,  3,  9,$FE,  8,  3,  9,$FE,  8
	dc.b   3,  6,  2,  2,  2,  7,$FE,  8,  3,  9,$FE,  8,  3,  9,$FE,  8
	dc.b   3,  9,$FE,  8,  3,  6,  2,  2,  2,  7,$FE,  8,  3,  9,$FE,  8
	dc.b   3,  9,$FE,  8,  3,  9,$FE,  8,  3,  6,  2,  2,  2,$FE,  9,  4
	dc.b   2,$FE,  8,  4,  2,$FE,  8,  4,  2,$FE,  8,  4,$FE,$2B,  2,$FF
	
	
 unk_31AC2:      
	dc.b   0             
	dc.b   0
	dc.b   0
	dc.b   3
	dc.b   0
	dc.b   6
	dc.b   0
	dc.b   9
	dc.b $FF
	dc.b $FF
	
 Hero_Slot_count:      
	dc.b   2         
	dc.b   2
	dc.b   2
	dc.b   3
	dc.b   5
	dc.b   4
	dc.b   6
	dc.b   7
	dc.b   7
	dc.b   7
	dc.b   8
	dc.b   8
	dc.b   7
	dc.b   6
	dc.b   4
	dc.b   8
	dc.b   8
	dc.b   8
	dc.b   8
	dc.b   8	
	
 aHp:            
	dc.b 'HP'              
	dc.b $FF
	dc.b   0	
	
 aMp:            
	dc.b 'MP'
	dc.b $FF
	dc.b   0
	
 aAt:            
	dc.b 'AT'             
	dc.b $FF
	dc.b   0
	
 aDf:            
	dc.b 'DF'              
	dc.b $FF
	dc.b   0
	
 aMv:            
	dc.b 'MV'             
	dc.b $FF
	dc.b   0
	
 aLv:            
	dc.b 'LV'              
	dc.b $FF
	dc.b   0
	
 aRange:         
	dc.b 'Range'     
	dc.b $FF
	
 aRevision:      
	dc.b 'Revision'         
	dc.b $FF
	dc.b   0
	
 aA:             
	dc.b 'A+'              
	dc.b $FF
	dc.b   0
	
 aD:             
	dc.b 'D+'              
	dc.b $FF
	dc.b   0
	
 aItem_0:        
	dc.b 'ITEM'
	dc.b $FF
	dc.b   0
	
 aPts:           
	dc.b 'Pts'
	dc.b $FF
	
 aAutomaticSelection:
	dc.b 'Automatic Selection'
	dc.b $FF
	
 IcoSize?:     
	dc.b   0                
	dc.b   5
	dc.b   0
	dc.b   0
	
 mnugfx?:     
	dc.b $FD,  0,$FC,  1,  5,  0,$FC,  1, $D,  0,$FC,  1,$15,  0,$FC,  1
	dc.b $1D,  0,$FC,  1,$FD,  0,  4,  1,$1D,  0,  4,  1,$FD,  0, $C,  1
	dc.b $1D,  0, $C,  1,$FD,  0,$14,  1,$1D,  0,$14,  1,$FD,  0,$1C,  1
	dc.b $1D,  0,$1C,  1,$FD,  0,$24,  1,$1D,  0,$24,  1,$FD,  0,$2C,  1
	dc.b $1D,  0,$2C,  1,$FD,  0,$34,  1,$1D,  0,$34,  1,$FD,  0,$3C,  1
	dc.b   5,  0,$3C,  1, $D,  0,$3C,  1,$15,  0,$3C,  1,$1D,  0,$3C,  0
	
 byte_31B92:     
	dc.b   0,  1,  0,  0,$FF,$FF,  0,  0,  0,  2,  0,  1,  0,  0,$FF,$FF
	dc.b $FF,$FE,$FF,$FF,  0,  0,  0,  1,  0,  3,  0,  2,  0,  1,  0,  0
	dc.b $FF,$FF,$FF,$FE,$FF,$FD,$FF,$FE,$FF,$FF,  0,  0,  0,  1,  0,  2
	dc.b   0,  0,$FF,$FF,  0,  0,  0,  1,  0,  0,  0,  1,  0,  2,  0,  1
	dc.b   0,  0,$FF,$FF,$FF,$FE,$FF,$FF,  0,  0,  0,  1,  0,  2,  0,  3
	dc.b   0,  2,  0,  1,  0,  0,$FF,$FF,$FF,$FE,$FF,$FD,$FF,$FE,$FF,$FF	
	
Enemyinfo:      
	dc.l Scenario_1_enemys             
	dc.l Scenario_2_enemys
	dc.l Scenario_3_enemys
	dc.l Scenario_4_enemys
	dc.l Scenario_5_enemys
	dc.l Scenario_6_enemys
	dc.l Scenario_7_enemys
	dc.l Scenario_8_enemys
	dc.l Scenario_9_enemys
	dc.l Scenario_10_enemys
	dc.l Scenario_11_enemys
	dc.l Scenario_12_enemys
	dc.l Scenario_13_enemys
	dc.l Scenario_14_enemys
	dc.l Scenario_15_enemys
	dc.l Scenario_16_enemys
	dc.l Scenario_17_enemys
	dc.l Scenario_18_enemys
	dc.l Scenario_19_enemys
	dc.l Scenario_20_enemys
	
Enemy_count:     
	incbin ScenarioData\Scenario_1\Enemy\Count
	incbin ScenarioData\Scenario_2\Enemy\Count
	incbin ScenarioData\Scenario_3\Enemy\Count
	incbin ScenarioData\Scenario_4\Enemy\Count
	incbin ScenarioData\Scenario_5\Enemy\Count
	incbin ScenarioData\Scenario_6\Enemy\Count
	incbin ScenarioData\Scenario_7\Enemy\Count
	incbin ScenarioData\Scenario_8\Enemy\Count
	incbin ScenarioData\Scenario_9\Enemy\Count
	incbin ScenarioData\Scenario_10\Enemy\Count
	incbin ScenarioData\Scenario_11\Enemy\Count
	incbin ScenarioData\Scenario_12\Enemy\Count
	incbin ScenarioData\Scenario_13\Enemy\Count
	incbin ScenarioData\Scenario_14\Enemy\Count
	incbin ScenarioData\Scenario_15\Enemy\Count
	incbin ScenarioData\Scenario_16\Enemy\Count
	incbin ScenarioData\Scenario_17\Enemy\Count
	incbin ScenarioData\Scenario_18\Enemy\Count
	incbin ScenarioData\Scenario_19\Enemy\Count
	incbin ScenarioData\Scenario_20\Enemy\Count		

 Scenario_1_enemys:
	incbin ScenarioData\Scenario_1\Enemy\0		
	incbin ScenarioData\Scenario_1\Enemy\1		
	incbin ScenarioData\Scenario_1\Enemy\2		
	incbin ScenarioData\Scenario_1\Enemy\3		
 Scenario_2_enemys:         
	incbin ScenarioData\Scenario_2\Enemy\4		
	incbin ScenarioData\Scenario_2\Enemy\5		
	incbin ScenarioData\Scenario_2\Enemy\6		
	incbin ScenarioData\Scenario_2\Enemy\7		
	incbin ScenarioData\Scenario_2\Enemy\8		
 Scenario_3_enemys:
	incbin ScenarioData\Scenario_3\Enemy\9		
	incbin ScenarioData\Scenario_3\Enemy\10		
	incbin ScenarioData\Scenario_3\Enemy\11		
	incbin ScenarioData\Scenario_3\Enemy\12		
	incbin ScenarioData\Scenario_3\Enemy\13		
	incbin ScenarioData\Scenario_3\Enemy\14		
	incbin ScenarioData\Scenario_3\Enemy\15		
 Scenario_4_enemys:
	incbin ScenarioData\Scenario_4\Enemy\16		
	incbin ScenarioData\Scenario_4\Enemy\17		
	incbin ScenarioData\Scenario_4\Enemy\18		
	incbin ScenarioData\Scenario_4\Enemy\19		
	incbin ScenarioData\Scenario_4\Enemy\20		
 Scenario_5_enemys:
	incbin ScenarioData\Scenario_5\Enemy\21		
	incbin ScenarioData\Scenario_5\Enemy\22		
	incbin ScenarioData\Scenario_5\Enemy\23		
	incbin ScenarioData\Scenario_5\Enemy\24		
	incbin ScenarioData\Scenario_5\Enemy\25		
 Scenario_6_enemys:
	incbin ScenarioData\Scenario_6\Enemy\26		
	incbin ScenarioData\Scenario_6\Enemy\27		
	incbin ScenarioData\Scenario_6\Enemy\28		
	incbin ScenarioData\Scenario_6\Enemy\29		
	incbin ScenarioData\Scenario_6\Enemy\30		
	incbin ScenarioData\Scenario_6\Enemy\31		
 Scenario_7_enemys:
	incbin ScenarioData\Scenario_7\Enemy\32		
	incbin ScenarioData\Scenario_7\Enemy\33		
	incbin ScenarioData\Scenario_7\Enemy\34		
	incbin ScenarioData\Scenario_7\Enemy\35		
	incbin ScenarioData\Scenario_7\Enemy\36		
	incbin ScenarioData\Scenario_7\Enemy\37		
 Scenario_8_enemys:
	incbin ScenarioData\Scenario_8\Enemy\38		
	incbin ScenarioData\Scenario_8\Enemy\39		
	incbin ScenarioData\Scenario_8\Enemy\40		
	incbin ScenarioData\Scenario_8\Enemy\41		
	incbin ScenarioData\Scenario_8\Enemy\42		
 Scenario_9_enemys:
	incbin ScenarioData\Scenario_9\Enemy\43		
	incbin ScenarioData\Scenario_9\Enemy\44		
	incbin ScenarioData\Scenario_9\Enemy\45		
	incbin ScenarioData\Scenario_9\Enemy\46		
	incbin ScenarioData\Scenario_9\Enemy\47		
 Scenario_10_enemys:
	incbin ScenarioData\Scenario_10\Enemy\48		
	incbin ScenarioData\Scenario_10\Enemy\49		
	incbin ScenarioData\Scenario_10\Enemy\50		
	incbin ScenarioData\Scenario_10\Enemy\51		
	incbin ScenarioData\Scenario_10\Enemy\52		
	incbin ScenarioData\Scenario_10\Enemy\53		
 Scenario_11_enemys:
	incbin ScenarioData\Scenario_11\Enemy\54		
	incbin ScenarioData\Scenario_11\Enemy\55		
	incbin ScenarioData\Scenario_11\Enemy\56		
	incbin ScenarioData\Scenario_11\Enemy\57		
	incbin ScenarioData\Scenario_11\Enemy\58		
 Scenario_12_enemys:
	incbin ScenarioData\Scenario_12\Enemy\59		
	incbin ScenarioData\Scenario_12\Enemy\60		
	incbin ScenarioData\Scenario_12\Enemy\61		
	incbin ScenarioData\Scenario_12\Enemy\62		
	incbin ScenarioData\Scenario_12\Enemy\63		
	incbin ScenarioData\Scenario_12\Enemy\64		
	incbin ScenarioData\Scenario_12\Enemy\65		
	incbin ScenarioData\Scenario_12\Enemy\66		
 Scenario_13_enemys:
	incbin ScenarioData\Scenario_13\Enemy\67		
	incbin ScenarioData\Scenario_13\Enemy\68		
	incbin ScenarioData\Scenario_13\Enemy\69		
	incbin ScenarioData\Scenario_13\Enemy\70		
	incbin ScenarioData\Scenario_13\Enemy\71		
	incbin ScenarioData\Scenario_13\Enemy\72		
	incbin ScenarioData\Scenario_13\Enemy\73		
 Scenario_14_enemys:      
	incbin ScenarioData\Scenario_14\Enemy\74		
 Scenario_15_enemys:
	incbin ScenarioData\Scenario_15\Enemy\75		
	incbin ScenarioData\Scenario_15\Enemy\76		
	incbin ScenarioData\Scenario_15\Enemy\77		
	incbin ScenarioData\Scenario_15\Enemy\78		
Scenario_16_enemys:
	incbin ScenarioData\Scenario_16\Enemy\79		
	incbin ScenarioData\Scenario_16\Enemy\80		
	incbin ScenarioData\Scenario_16\Enemy\81		
	incbin ScenarioData\Scenario_16\Enemy\82		
	incbin ScenarioData\Scenario_16\Enemy\83		
	incbin ScenarioData\Scenario_16\Enemy\84		
	incbin ScenarioData\Scenario_16\Enemy\85		
	incbin ScenarioData\Scenario_16\Enemy\86		
Scenario_17_enemys:
	incbin ScenarioData\Scenario_17\Enemy\87		
	incbin ScenarioData\Scenario_17\Enemy\88		
	incbin ScenarioData\Scenario_17\Enemy\89		
	incbin ScenarioData\Scenario_17\Enemy\90		
	incbin ScenarioData\Scenario_17\Enemy\91		
	incbin ScenarioData\Scenario_17\Enemy\92		
	incbin ScenarioData\Scenario_17\Enemy\93		
	incbin ScenarioData\Scenario_17\Enemy\94		
 Scenario_18_enemys:
	incbin ScenarioData\Scenario_18\Enemy\95		
	incbin ScenarioData\Scenario_18\Enemy\96		
	incbin ScenarioData\Scenario_18\Enemy\97		
	incbin ScenarioData\Scenario_18\Enemy\98		
	incbin ScenarioData\Scenario_18\Enemy\99		
	incbin ScenarioData\Scenario_18\Enemy\100		
	incbin ScenarioData\Scenario_18\Enemy\101		
	incbin ScenarioData\Scenario_18\Enemy\102		
 Scenario_19_enemys:
	incbin ScenarioData\Scenario_19\Enemy\103		
	incbin ScenarioData\Scenario_19\Enemy\104		
	incbin ScenarioData\Scenario_19\Enemy\105		
	incbin ScenarioData\Scenario_19\Enemy\106		
	incbin ScenarioData\Scenario_19\Enemy\107		
	incbin ScenarioData\Scenario_19\Enemy\108		
	incbin ScenarioData\Scenario_19\Enemy\109		
	incbin ScenarioData\Scenario_19\Enemy\110		
 Scenario_20_enemys:
	incbin ScenarioData\Scenario_20\Enemy\111		
	incbin ScenarioData\Scenario_20\Enemy\112		
	incbin ScenarioData\Scenario_20\Enemy\113		
	incbin ScenarioData\Scenario_20\Enemy\114		
	incbin ScenarioData\Scenario_20\Enemy\115		
	incbin ScenarioData\Scenario_20\Enemy\116		
	incbin ScenarioData\Scenario_20\Enemy\117		

 Additional_units:    
	dc.l f32AB6            
	dc.l f32AD4
	dc.l f32B10
	dc.l f32B4C
	dc.l f32B88
	dc.l f32BC4
	dc.l f32BE2
	dc.l f32C1E
	dc.l f32C3C
	dc.l f32C78
	dc.l f32D2C
	dc.l f32D4A
	dc.l f32D68
	dc.l Friend_params
	dc.l Friend_params
	dc.l Friend_params
	dc.l Friend_params
	dc.l Friend_params
	dc.l Friend_params
	dc.l Friend_params
	
 word_32A8E:     
	incbin ScenarioData\Additional\ScenarioUnits\1\Count	
	incbin ScenarioData\Additional\ScenarioUnits\2\Count	
	incbin ScenarioData\Additional\ScenarioUnits\3\Count	
	incbin ScenarioData\Additional\ScenarioUnits\4\Count	
	incbin ScenarioData\Additional\ScenarioUnits\5\Count	
	incbin ScenarioData\Additional\ScenarioUnits\6\Count	
	incbin ScenarioData\Additional\ScenarioUnits\7\Count	
	incbin ScenarioData\Additional\ScenarioUnits\8\Count	
	incbin ScenarioData\Additional\ScenarioUnits\9\Count	
	incbin ScenarioData\Additional\ScenarioUnits\10\Count	
	incbin ScenarioData\Additional\ScenarioUnits\11\Count	
	incbin ScenarioData\Additional\ScenarioUnits\12\Count	
	incbin ScenarioData\Additional\ScenarioUnits\13\Count	
	dc.w 6
	dc.w 8
	dc.w 8
	dc.w 8
	dc.w 8
	dc.w 8
	dc.w 8

 f32AB6:
	incbin ScenarioData\Additional\ScenarioUnits\1\0	
 f32AD4:
	incbin ScenarioData\Additional\ScenarioUnits\2\1	
	incbin ScenarioData\Additional\ScenarioUnits\2\2	
 f32B10:
	incbin ScenarioData\Additional\ScenarioUnits\3\3	
	incbin ScenarioData\Additional\ScenarioUnits\3\4	
 f32B4C:
	incbin ScenarioData\Additional\ScenarioUnits\4\5	
	incbin ScenarioData\Additional\ScenarioUnits\4\6	
 f32B88:
	incbin ScenarioData\Additional\ScenarioUnits\5\7	
	incbin ScenarioData\Additional\ScenarioUnits\5\8	
 f32BC4:
	incbin ScenarioData\Additional\ScenarioUnits\6\9	
 f32BE2:
	incbin ScenarioData\Additional\ScenarioUnits\7\10	
	incbin ScenarioData\Additional\ScenarioUnits\7\11	
 f32C1E:
	incbin ScenarioData\Additional\ScenarioUnits\8\12	
 f32C3C:
	incbin ScenarioData\Additional\ScenarioUnits\9\13	
	incbin ScenarioData\Additional\ScenarioUnits\9\14	
 f32C78:
	incbin ScenarioData\Additional\ScenarioUnits\10\15	
	incbin ScenarioData\Additional\ScenarioUnits\10\16	
	incbin ScenarioData\Additional\ScenarioUnits\10\17	
	incbin ScenarioData\Additional\ScenarioUnits\10\18	
	incbin ScenarioData\Additional\ScenarioUnits\10\19	
	incbin ScenarioData\Additional\ScenarioUnits\10\20	
 f32D2C:
	incbin ScenarioData\Additional\ScenarioUnits\11\21	
 f32D4A:
	incbin ScenarioData\Additional\ScenarioUnits\12\22	
 f32D68:
	incbin ScenarioData\Additional\ScenarioUnits\13\23	

 Friend_params:    
	dc.l Scenario_1_friends            
	dc.l Scenario_2_friends
	dc.l Scenario_3_friends
	dc.l Scenario_7_friends
	dc.l Scenario_7_friends
	dc.l Scenario_7_friends
	dc.l Scenario_7_friends
	dc.l Scenario_13_friends
	dc.l Scenario_13_friends
	dc.l Scenario_13_friends
	dc.l Scenario_13_friends
	dc.l Scenario_13_friends
	dc.l Scenario_13_friends
	dc.l Scenario_14_friends
	dc.l Scenario_15_friends
	dc.l off_32FA2
	dc.l off_32FA2
	dc.l off_32FA2
	dc.l off_32FA2
	dc.l off_32FA2

 Friend_count:     
	incbin ScenarioData\Scenario_1\Friend\Count
	incbin ScenarioData\Scenario_2\Friend\Count
	incbin ScenarioData\Scenario_3\Friend\Count
	incbin ScenarioData\Scenario_4\Friend\Count
	incbin ScenarioData\Scenario_5\Friend\Count
	incbin ScenarioData\Scenario_6\Friend\Count
	incbin ScenarioData\Scenario_7\Friend\Count
	incbin ScenarioData\Scenario_8\Friend\Count
	incbin ScenarioData\Scenario_9\Friend\Count
	incbin ScenarioData\Scenario_10\Friend\Count
	incbin ScenarioData\Scenario_11\Friend\Count
	incbin ScenarioData\Scenario_12\Friend\Count
	incbin ScenarioData\Scenario_13\Friend\Count
	incbin ScenarioData\Scenario_14\Friend\Count
	incbin ScenarioData\Scenario_15\Friend\Count
	incbin ScenarioData\Scenario_16\Friend\Count
	incbin ScenarioData\Scenario_17\Friend\Count
	incbin ScenarioData\Scenario_18\Friend\Count
	incbin ScenarioData\Scenario_19\Friend\Count
	incbin ScenarioData\Scenario_20\Friend\Count	
	
 Scenario_1_friends: 
 	incbin ScenarioData\Scenario_1\Friend\0
 	incbin ScenarioData\Scenario_1\Friend\1
 	incbin ScenarioData\Scenario_1\Friend\2
 	incbin ScenarioData\Scenario_1\Friend\3
 Scenario_2_friends:
 	incbin ScenarioData\Scenario_2\Friend\4
 Scenario_3_friends:
 	incbin ScenarioData\Scenario_3\Friend\5
 	incbin ScenarioData\Scenario_3\Friend\6
 Scenario_7_friends:
 	incbin ScenarioData\Scenario_7\Friend\7
 Scenario_13_friends:
 	incbin ScenarioData\Scenario_13\Friend\8
 Scenario_14_friends:
	incbin ScenarioData\Scenario_14\Friend\9
 	incbin ScenarioData\Scenario_14\Friend\10
  Scenario_15_friends:
	incbin ScenarioData\Scenario_15\Friend\11
	incbin ScenarioData\Scenario_15\Friend\12
	incbin ScenarioData\Scenario_15\Friend\13

 off_32FA2:
 	dc.l f32FF2
	dc.l f32FF2
	dc.l f32FF2
	dc.l f32FFA
	dc.l f32FFA
	dc.l f32FFA
	dc.l f32FFA
	dc.l f32FFA
	dc.l f32FFA
	dc.l f32FFA
	dc.l f32FFA
	dc.l f32FFA
	dc.l f3301A
	dc.l f3301A
	dc.l f3301A
	dc.l f3301A
	dc.l f3301A
	dc.l f3301A
	dc.l f3301A
	dc.l f3301A
	
 f32FF2:
	dc.w   $11,  $19,  $23,    8; 0
 f32FFA:
	dc.w     5,    3,  $16,   $D,    9,    3,  $1A,   $D,    6,    8,  $16,  $11,    8,    8,  $1A,  $11; 0
 f3301A:
	dc.w    $A,   $A,   $C,   $C,   $F,   $F,  $12,  $12,  $14,  $14,  $19,  $19; 0
	
byte_33032:     
	dc.b   0                ; DATA XREF: sub_15210+12o
	dc.b   2
	dc.b   0
	dc.b   8
	dc.b   0
	dc.b   8
	dc.b $FA ; ·
	dc.b $6E ; n
	dc.b $FE ; ¦
	dc.b   6
	dc.b $71 ; q
	dc.b $74 ; t   
	dc.b $6F
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $70 ; p
	dc.b $FE ; ¦
	dc.b   6
	dc.b $73 ; s
	dc.b $76 ; v
	dc.b $FF

unk_33062:      
	dc.b   0                ; DATA XREF: sub_14E9E+38o
	dc.b  $A
	dc.b   0
	dc.b  $A
	dc.b $C0 ; L
	dc.b   0
	dc.b $FF
	dc.b $FF
unk_3306A: 
	dc.b   0                ; DATA XREF: sub_15186+16o
	dc.b  $D
	dc.b   0
	dc.b $10
	dc.b $C0 ; L
	dc.b   0
	dc.b $FF
	dc.b $FF
	
unk_33072:      
	dc.b   0                ; DATA XREF: sub_14E9E+A2o
	dc.b $18
	dc.b   0
	dc.b  $D
	dc.b $C0 ; L
	dc.b   0
	dc.b   0
	dc.b   4
	dc.l byte_33080
	dc.b $FF
	dc.b $FF
	
byte_33080:     dc.b 0                  ; DATA XREF: ROM:0003307Ao
                dc.b   2
                dc.b   0
                dc.b   2
	
 aCanMoveUpToNextLevel_:
	dc.b '           can move',$D,$D,'  up to next level.',$D,$D
	dc.b $FF
	dc.b   0
	
 byte_330B0:
	dc.b   0,$19,  0,  8,$C0,  0,  0,  4,  0,  3,$30,$BE,$FF,$FF,  0,  2
	dc.b   0,  2
	
 aMovedUpTo:
	dc.b '         moved up to'
	dc.b $FF
	dc.b   0
	
 byte_330D8:     
	dc.b   0
	dc.b   7
	dc.b   0
	dc.b  $A
	dc.b $FF
	dc.b $FF

				 
 unk_330DE:      dc.b   0                ; DATA XREF: ROM:00015250o
                 dc.b $10
                 dc.b   0
                 dc.b   7
                 dc.b $C0 ; L
                 dc.b   0
                 dc.b   0
                 dc.b   4
                 dc.l byte_330EC
                 dc.b $FF
                 dc.b $FF
 byte_330EC:     dc.b 0                  ; DATA XREF: ROM:000330E6o
                 dc.b   2
                 dc.b   0
                 dc.b   2
	
	
 aWasUpgraded_:  
	dc.b '        was',$D,$D,'upgraded.',$D,$D
	dc.b $FF
	dc.b   0
	
	
 sub_3310A:                              
	movem.l d1-a6,-(sp)
	clr.w   ($FFFFE8B8).l
	tst.w   ($FFFFE8D8).l
	bne.w   loc_3318C
	cmpi.w  #0,($FFFFAEAC).w
	beq.s   loc_33154
	lea     (off_335B8).l,a2

 loc_3312C:                              
	movea.l (a2)+,a3
	cmpa.l  #$FFFFFFFF,a3
	beq.s   loc_33154
	jsr     (a3)
	cmpi.w  #0,d0
	beq.s   loc_3314C
	move.w  #1,($FFFFE8B8).l
	movea.l (a2)+,a3
	jsr     (a3)
	bra.s   loc_3318C
	
 loc_3314C:                              
	adda.l  #4,a2
	bra.s   loc_3312C
 ; ---------------------------------------------------------------------------

 loc_33154:                              
	 lea     (lvlscripts).l,a1
	 move.w  ($FFFFAEAC).w,d1
	 lsl.w   #2,d1
	 movea.l (a1,d1.w),a2

 loc_33164:                              
	movea.l (a2)+,a3
	cmpa.l  #$FFFFFFFF,a3
	beq.s   loc_3318C
	jsr     (a3)
	cmpi.w  #0,d0
	beq.s   loc_33184
	move.w  #1,($FFFFE8B8).l
	movea.l (a2)+,a3
	jsr     (a3)
	bra.s   loc_33164
 ; ---------------------------------------------------------------------------

 loc_33184:                             
	adda.l  #4,a2
	bra.s   loc_33164
 ; ---------------------------------------------------------------------------

 loc_3318C:                             
	move.w  ($FFFFE8B8).l,d0
	movem.l (sp)+,d1-a6
	rts
 ; ---------------------------------------------------------------------------
 
	
 lvlscripts:     dc.l Level1_scripts     ; 0 ; DATA XREF: sub_3310A:loc_33154o
                 dc.l Level2_scripts
                 dc.l Level3_scripts
                 dc.l Level4_scripts
                 dc.l Level6_scripts
                 dc.l Level7_scripts
                 dc.l Level8_scripts
                 dc.l Level9_scripts
                 dc.l Level10_scripts
                 dc.l Level11_scripts
                 dc.l Level12_scripts
                 dc.l Level13_scripts
                 dc.l Level14_scripts
                 dc.l Level15_scripts
                 dc.l Level16_scripts
                 dc.l Level17_scripts
                 dc.l Level18_scripts
                 dc.l Level19_scripts
                 dc.l Level20_scripts
                 dc.l off_33594
 Level1_scripts: dc.l IF_Turn_Eq_1          ; 0 ; DATA XREF: ROM:lvlscriptso
                dc.l sub_179E8
                dc.l Sabra_vs_Enemy
                dc.l sub_17A4A
                dc.l Calais_vs_Enemy
                dc.l sub_17A6C
                dc.l Tiberon_vs_Enemy
                dc.l sub_17A86
                dc.l Alfador_vs_Enemy
                dc.l sub_17AF8
                dc.l AllEnemyDied
                dc.l sub_17B12
                dc.l GarretDead
                dc.l sub_17B3E
                dc.l if_Edge_of_Map
                dc.l sub_17B6E
                dc.l if_turn2
                dc.l sub_17A02
                dc.l Calais_Unit_vs_Enemy_Leader
                dc.l sub_17ACC
                dc.l $FFFFFFFF
 Level2_scripts: dc.l s2_If_turn1          ; 0 ; DATA XREF: ROM:0003319Co
                 dc.l sub_17B94
                 dc.l s2_If_mina_top_of_map
                 dc.l sub_17BAE
                 dc.l s2_endScen
                 dc.l sub_17BC8
                 dc.l s2_Mina_dead
                 dc.l sub_17C00
                 dc.l s2_turn13
                 dc.l sub_17C30
                 dc.l s2_allEnemyDead
                 dc.l sub_17BC8
                 dc.l s2_mina_attacked
                 dc.l sub_17C5C
                 dc.l $FFFFFFFF
 Level3_scripts: dc.l sub_15E40          ; 0 ; DATA XREF: ROM:000331A0o
                 dc.l sub_17C76
                 dc.l sub_15E78
                 dc.l sub_17C92
                 dc.l sub_15EBE
                 dc.l sub_17CAC
                 dc.l sub_15F0E
                 dc.l sub_17CD2
                 dc.l sub_15F72
                 dc.l sub_17D02
                 dc.l $FFFFFFFF
 Level4_scripts: dc.l sub_15FAA          ; DATA XREF: ROM:000331A4o
 Level5_scripts: dc.l sub_17D1C          ; 0
                 dc.l sub_15FE2
                 dc.l sub_17D36
                 dc.l loc_16028
                 dc.l sub_17D50
                 dc.l sub_1605A
                 dc.l sub_17D7C
                 dc.l sub_1609A
                 dc.l sub_17D7C
                 dc.l sub_160F0
                 dc.l sub_17DBA
                 dc.l $FFFFFFFF
 Level6_scripts: dc.l sub_1613E          ; DATA XREF: ROM:000331A8o
                 dc.l sub_17DD4
                 dc.l sub_16176
                 dc.l sub_17DEE
                 dc.l sub_161BC
                 dc.l sub_17E08
                 dc.l sub_16202
                 dc.l sub_17E30
                 dc.l sub_1623A
                 dc.l sub_17E4A
                 dc.l $FFFFFFFF
 Level7_scripts: dc.l sub_16272          ; 0 ; DATA XREF: ROM:000331ACo
                 dc.l sub_17E64
                 dc.l sub_162AA
                 dc.l sub_17E7E
                 dc.l sub_162F0
                 dc.l sub_17E98
                 dc.l sub_16330
                 dc.l sub_17ED2
                 dc.l $FFFFFFFF
 Level8_scripts: dc.l sub_16368          ; DATA XREF: ROM:000331B0o
                 dc.l sub_17EEC
                 dc.l sub_163A0
                 dc.l sub_17F06
                 dc.l sub_163E8
                 dc.l sub_17F20
                 dc.l sub_1642E
                 dc.l sub_17F3A
                 dc.l sub_16466
                 dc.l sub_17F66
                 dc.l sub_164AC
                 dc.l sub_17F8E
                 dc.l sub_16508
                 dc.l sub_17FB8
                 dc.l sub_1656C
                 dc.l sub_17FD2
                 dc.l $FFFFFFFF
 Level9_scripts: dc.l sub_165A4          ; DATA XREF: ROM:000331B4o
                 dc.l sub_17FEC
                 dc.l sub_165DC
                 dc.l sub_18006
                 dc.l sub_16614
                 dc.l sub_18032
                 dc.l sub_1665A
                 dc.l sub_1805A
                 dc.l $FFFFFFFF
 Level10_scripts:dc.l sub_166B0          ; DATA XREF: ROM:000331B8o
                 dc.l sub_18086
                 dc.l sub_166E8
                 dc.l sub_180A0
                 dc.l sub_16720
                 dc.l sub_180CC
                 dc.l sub_16758
                 dc.l sub_180F8
                 dc.l sub_1679E
                 dc.l sub_18112
                 dc.l sub_167E4
                 dc.l sub_1813A
                 dc.l sub_1682A
                 dc.l sub_1818E
                 dc.l $FFFFFFFF
 Level11_scripts:dc.l sub_16880          ; DATA XREF: ROM:000331BCo
                 dc.l sub_181B6
                 dc.l loc_168B8
                 dc.l sub_181D0
                 dc.l sub_168EA
                 dc.l sub_181EA
                 dc.l sub_16922
                 dc.l sub_18216
                 dc.l sub_16968
                 dc.l sub_18232
                 dc.l sub_169A8
                 dc.l sub_1826C
                 dc.l $FFFFFFFF
 Level12_scripts:dc.l sub_169E0          ; DATA XREF: ROM:000331C0o
                 dc.l sub_18288
                 dc.l sub_16A18
                 dc.l sub_182A2
                 dc.l sub_16A50
                 dc.l sub_182CE
                 dc.l sub_16A88
                 dc.l sub_182FA
                 dc.l sub_16ACE
                 dc.l sub_18314
                 dc.l sub_16B14
                 dc.l sub_1832E
                 dc.l sub_16B70
                 dc.l sub_18380
                 dc.l $FFFFFFFF
 Level13_scripts:dc.l sub_16BAA          ; DATA XREF: ROM:000331C4o
                 dc.l sub_1839A
                 dc.l sub_16BE2
                 dc.l sub_183E0
                 dc.l sub_16C22
                 dc.l sub_183FA
                 dc.l $FFFFFFFF
 Level14_scripts:dc.l sub_16C62          ; DATA XREF: ROM:000331C8o
                 dc.l sub_1843E
                 dc.l sub_16C9A
                 dc.l sub_18458
                 dc.l sub_16CEE
                 dc.l sub_1852A
                 dc.l sub_16D36
                 dc.l sub_18544
                 dc.l $FFFFFFFF
 Level15_scripts:dc.l sub_16D8C          ; DATA XREF: ROM:000331CCo
                 dc.l sub_1857E
                 dc.l sub_16DC4
                 dc.l sub_18598
                 dc.l sub_16E04
                 dc.l sub_185C4
                 dc.l sub_16E56
                 dc.l sub_185DE
                 dc.l sub_16E8E
                 dc.l sub_185F8
                 dc.l sub_16EE4
                 dc.l sub_1861E
                 dc.l $FFFFFFFF
 Level16_scripts:dc.l loc_16F1C          ; DATA XREF: ROM:000331D0o
                 dc.l sub_18638
                 dc.l loc_16F54
                 dc.l sub_18652
                 dc.l sub_16F9C
                 dc.l sub_1866C
                 dc.l sub_16FE4
                 dc.l nullsub_7
                 dc.l sub_16FEA
                 dc.l sub_18688
                 dc.l sub_1702A
                 dc.l sub_186C0
                 dc.l sub_17078
                 dc.l sub_186DA
                 dc.l sub_170C6
                 dc.l sub_186F4
                 dc.l sub_17106
                 dc.l sub_1871A
                 dc.l sub_17158
                 dc.l sub_18744
                 dc.l $FFFFFFFF
 Level17_scripts:dc.l sub_17190          ; DATA XREF: ROM:000331D4o
                 dc.l sub_1875E
                 dc.l sub_171C8
                 dc.l sub_18778
                 dc.l sub_1721C
                 dc.l sub_18824
                 dc.l sub_17270
                 dc.l sub_188C2
                 dc.l $FFFFFFFF
 Level18_scripts:dc.l sub_172B0          ; DATA XREF: ROM:000331D8o
                 dc.l sub_188F8
                 dc.l sub_172E8
                 dc.l sub_18912
                 dc.l sub_1732E
                 dc.l sub_1892C
                 dc.l $FFFFFFFF
 Level19_scripts:dc.l sub_17384          ; DATA XREF: ROM:000331DCo
                 dc.l sub_18952
                 dc.l sub_173BC
                 dc.l sub_1896C
                 dc.l sub_1741E
                 dc.l sub_18986
                 dc.l sub_17464
                 dc.l sub_189A0
                 dc.l $FFFFFFFF
 Level20_scripts:dc.l sub_174A4          ; DATA XREF: ROM:000331E0o
                 dc.l sub_18A26
                 dc.l sub_174DC
                 dc.l sub_18A40
                 dc.l sub_17522
                 dc.l sub_18A5A
                 dc.l sub_17568
                 dc.l sub_18A74
                 dc.l sub_175A8
                 dc.l sub_18A9A
                 dc.l $FFFFFFFF
 off_33594:      dc.l sub_175DA          ; DATA XREF: ROM:000331E4o
                 dc.l sub_18AB4
                 dc.l sub_17612
                 dc.l sub_18ACE
                 dc.l sub_17658
                 dc.l sub_18AE8
                 dc.l sub_17698
                 dc.l sub_18B14
                 dc.l $FFFFFFFF
 off_335B8:      dc.l sub_176DE          ; 0 ; DATA XREF: sub_3310A+1Co
                 dc.l sub_18B3A
                 dc.l sub_1772E
                 dc.l sub_18B6A
                 dc.l sub_1777A
                 dc.l sub_18B8A
                 dc.l sub_177C6
                 dc.l sub_18BAA
                 dc.l sub_17812
                 dc.l sub_18BCA
                 dc.l sub_1785E
                 dc.l sub_18BEA
                 dc.l sub_178AA
                 dc.l sub_18C0A
                 dc.l sub_178F6
                 dc.l sub_18C2A
                 dc.l sub_17942
                 dc.l sub_18C4A
                 dc.l sub_1798E
                 dc.l sub_18C6A
                 dc.l $FFFFFFFF
	   
unk_3360C:     
	dc.b 0                  ; DATA XREF: ROM:0001562Co
	dc.b $23 ; #
	dc.b   0
	dc.b  $D
	dc.b $C0
	dc.b   0
	dc.b 0
	dc.b   5
	dc.l byte_3361A
	dc.b $FF
	dc.b $FF
byte_3361A:     
	dc.b 0                  ; DATA XREF: sub_3310A+50Ao
	dc.b   1
	dc.b   0
	dc.b   1
	dc.b $80 ; А
	dc.b   0
	dc.b 0
	dc.b   0
	dc.b   0
	dc.b   2
	dc.b   0
	dc.b   8
	dc.b 0
	dc.b   8
	dc.b $FA ; ·
	dc.b $6E ; n
	dc.b $FE ; ¦
	dc.b   6
	dc.b $71 ; q
	dc.b $74 ; t
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F
	dc.b $FE ; ¦
	dc.b 6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F
	dc.b $FE ; ¦
	dc.b 6
	dc.b   0
	dc.b $75 ; u
	dc.b $6F ; o
	dc.b $FE ; ¦
	dc.b   6
	dc.b   0
	dc.b $75 ; u
	dc.b $70
	dc.b $FE ; ¦
	dc.b 6
	dc.b $73 ; s
	dc.b $76 ; v
	dc.b $FF
	
	
IF_Turn_Eq_1:
				movem.l d1-a6,-(sp)
				move.w  (dialogFlags).l,d1
				btst    #0,d1
				bne.s   loc_3366A
				cmpi.w  #1,(CurTurnNum).w
				beq.s   loc_33670
loc_3366A:                             
                 move.w  #0,d0
                 bra.s   loc_33684
 ; ---------------------------------------------------------------------------
	
	
loc_33670:                              ; CODE XREF: sub_3310A+55Ej
                 tst.w   (CurTurnNum).w
                 beq.s   loc_3366A
                 move.w  #1,d0
                 bset    #0,d1
                 move.w  d1,(dialogFlags).l

 loc_33684:                              ; CODE XREF: sub_3310A+564j
                 movem.l (sp)+,d1-a6
                 rts
	 ; dc.b   1
	; dc.b   1
 ; ---------------------------------------------------------------------------
Sabra_vs_Enemy:  
				 movem.l d1-a6,-(sp)
                 move.w  (dialogFlags).l,d1
                 btst    #1,d1
                 bne.s   loc_336C6
                 tst.w   (FighingUnitIndex).w
                 bne.s   loc_336A8
                 cmpi.w  #4,(WhoAttack).w
                 beq.s   loc_336B6

 loc_336A8:                              ; CODE XREF: sub_3310A+594j
                 tst.w   (AttackedUnitIndex).w
                 bne.s   loc_336C6
                 cmpi.w  #4,(WhoAttacked).w
                 bne.s   loc_336C6

 loc_336B6:                              ; CODE XREF: sub_3310A+59Cj
                 move.w  #1,d0
                 bset    #1,d1
                 move.w  d1,(dialogFlags).l
                 bra.s   loc_336CA
 ; ---------------------------------------------------------------------------

 loc_336C6:                              ; CODE XREF: sub_3310A+58Ej
                                         ; sub_3310A+5A2j ...
                 move.w  #0,d0

 loc_336CA:                              ; CODE XREF: sub_3310A+5BAj
                 movem.l (sp)+,d1-a6
                 rts
 ; ---------------------------------------------------------------------------
Calais_vs_Enemy:
				movem.l d1-a6,-(sp)
    			move.w  (dialogFlags).l,d1
                btst    #2,d1
                bne.s   loc_3370C
                tst.w   (FighingUnitIndex).w
                bne.s   loc_336EE
                cmpi.w  #3,(WhoAttack).w
                beq.s   loc_336FC

 loc_336EE:                              ; CODE XREF: sub_3310A+5DAj
                 tst.w   (AttackedUnitIndex).w
                 bne.s   loc_3370C
                 cmpi.w  #3,(WhoAttacked).w
                 bne.s   loc_3370C

 loc_336FC:                              ; CODE XREF: sub_3310A+5E2j
                 move.w  #1,d0
                 bset    #2,d1
                 move.w  d1,(dialogFlags).l
                 bra.s   loc_33710
 ; ---------------------------------------------------------------------------

 loc_3370C:                              ; CODE XREF: sub_3310A+5D4j
                                         ; sub_3310A+5E8j ...
                 move.w  #0,d0

 loc_33710:                              ; CODE XREF: sub_3310A+600j
                 movem.l (sp)+,d1-a6
                 rts
 ; ---------------------------------------------------------------------------
Tiberon_vs_Enemy:
				 movem.l d1-a6,-(sp)
                 move.w  (dialogFlags).l,d1
                 btst    #3,d1
                 bne.s   loc_33752
                 tst.w   (FighingUnitIndex).w
                 bne.s   loc_33734
                 cmpi.w  #5,(WhoAttack).w
                 beq.s   loc_33742

 loc_33734:                              ; CODE XREF: sub_3310A+620j
                 tst.w   (AttackedUnitIndex).w
                 bne.s   loc_33752
                 cmpi.w  #5,(WhoAttacked).w
                 bne.s   loc_33752

 loc_33742:                              ; CODE XREF: sub_3310A+628j
                 move.w  #1,d0
                 bset    #3,d1
                 move.w  d1,(dialogFlags).l
                 bra.s   loc_33756
 ; ---------------------------------------------------------------------------

 loc_33752:                              ; CODE XREF: sub_3310A+61Aj
                                         ; sub_3310A+62Ej ...
                 move.w  #0,d0

 loc_33756:                              ; CODE XREF: sub_3310A+646j
                 movem.l (sp)+,d1-a6
                 rts
 ; ---------------------------------------------------------------------------
Alfador_vs_Enemy:
				 movem.l d1-a6,-(sp)
                 move.w  (dialogFlags).l,d1
                 btst    #4,d1
                 bne.s   loc_33798
                 tst.w   (FighingUnitIndex).w
                 bne.s   loc_3377A
                 cmpi.w  #2,(WhoAttack).w
                 beq.s   loc_33788

 loc_3377A:                              ; CODE XREF: sub_3310A+666j
                 tst.w   (AttackedUnitIndex).w
                 bne.s   loc_33798
                 cmpi.w  #2,(WhoAttacked).w
                 bne.s   loc_33798

 loc_33788:                              ; CODE XREF: sub_3310A+66Ej
                 move.w  #1,d0
                 bset    #4,d1
                 move.w  d1,(dialogFlags).l
                 bra.s   loc_3379C
 ; ---------------------------------------------------------------------------

 loc_33798:                              ; CODE XREF: sub_3310A+660j
                                         ; sub_3310A+674j ...
                 move.w  #0,d0

 loc_3379C:                              ; CODE XREF: sub_3310A+68Cj
                 movem.l (sp)+,d1-a6
                 rts
 ; ---------------------------------------------------------------------------
AllEnemyDied:
                movem.l d1-a6,-(sp)
                move.w  (dialogFlags).l,d3
                btst    #5,d3
                bne.s   loc_337EE
                lea     (UnitsAddr).l,a1
                adda.l  #$200,a1
                lea     (EnemyArmyCnt).l,a2
                move.w  (a2),d1
                 subq.w  #1,d1

 loc_337C8:                              ; CODE XREF: sub_3310A+6D0j
                 movea.l (a1),a2
                 move.b  2(a2),d2
                 btst    #1,d2
                 beq.s   loc_337EE
                 adda.l  #$40,a1 ; '@'
                 dbf     d1,loc_337C8
                 move.w  #1,d0
                 bset    #5,d3
                 move.w  d3,(dialogFlags).l
                 bra.s   loc_337F2
 ; ---------------------------------------------------------------------------

 loc_337EE:                              ; CODE XREF: sub_3310A+6A6j
                                         ; sub_3310A+6C8j
                 move.w  #0,d0

 loc_337F2:                              ; CODE XREF: sub_3310A+6E2j
                 movem.l (sp)+,d1-a6
                 rts
 ; ---------------------------------------------------------------------------
 GarretDead:
				 movem.l d1-a6,-(sp)
                 move.w  (dialogFlags).l,d1
                 btst    #6,d1
                 bne.s   loc_3383A
                 lea     (UnitsAddr).l,a1
                 movea.l (a1),a2
                 move.b  2(a2),d2
                 btst    #1,d2
                 beq.s   loc_3383A
                 move.w  #1,d0
                 bset    #6,d1
                 move.w  d1,(dialogFlags).l
                 move.w  (AllyDead).l,d1
                 bset    #0,d1
                 move.w  d1,(AllyDead).l
                 bra.s   loc_3383E
 ; ---------------------------------------------------------------------------

 loc_3383A:                              ; CODE XREF: sub_3310A+6FCj
                                         ; sub_3310A+70Ej
                 move.w  #0,d0

 loc_3383E:                              ; CODE XREF: sub_3310A+72Ej
                 movem.l (sp)+,d1-a6
                 rts
 ; ---------------------------------------------------------------------------
 if_Edge_of_Map: 
				 movem.l d1-a6,-(sp)
                 move.w  (dialogFlags).l,d1
                 btst    #7,d1
                 bne.s   loc_3387E
                 lea     (UnitsAddr).l,a1
                 movea.l (a1),a2
                 move.b  2(a2),d2
                 btst    #0,d2
                 beq.s   loc_3387E
                 move.b  $B(a2),d2
                 beq.s   loc_33884
                 cmpi.b  #$1F,d2
                 beq.s   loc_33884
                 move.b  $C(a2),d2
                 beq.s   loc_33884
                 cmpi.b  #$1F,d2
                 beq.s   loc_33884

 loc_3387E:                              ; CODE XREF: sub_3310A+748j
                                         ; sub_3310A+75Aj
                 move.w  #0,d0
                 bra.s   loc_33892
 ; ---------------------------------------------------------------------------

 loc_33884:                              ; CODE XREF: sub_3310A+760j
                                         ; sub_3310A+766j ...
                 move.w  #1,d0
                 bset    #7,d1
                 move.w  d1,(dialogFlags).l

 loc_33892:                              ; CODE XREF: sub_3310A+778j
                 movem.l (sp)+,d1-a6
                 rts
 ; ---------------------------------------------------------------------------
 if_turn2:
                 movem.l d1-a6,-(sp)
                 move.w  (dialogFlags).l,d1
                 btst    #8,d1
                 bne.s   loc_338B0
                 cmpi.w  #2,(CurTurnNum).w
                 beq.s   loc_338B6

 loc_338B0:                              ; CODE XREF: sub_3310A+79Cj
                                         ; sub_3310A+7B0j
                 move.w  #0,d0
                 bra.s   loc_338CA
 ; ---------------------------------------------------------------------------

 loc_338B6:                              ; CODE XREF: sub_3310A+7A4j
                 tst.w   (CurTurnNum).w
                 beq.s   loc_338B0
                 move.w  #1,d0
                 bset    #8,d1
                 move.w  d1,(dialogFlags).l

 loc_338CA:                              ; CODE XREF: sub_3310A+7AAj
                 movem.l (sp)+,d1-a6
                 rts	

;include 'Text.asm'				 