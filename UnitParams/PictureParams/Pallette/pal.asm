 Palette1: ;Garret
    dc.w  0,$444,$666,$CAA,$6E8,$24E,$228, 4,$EC2,$E00,$EE
	dc.w $46,$26A,$6AC, 0,$AEE
	; dc.w $000
	; dc.w $22E
	; dc.w $A82
	; dc.w $226
	; dc.w $48C
	; dc.w $8EE
	; dc.w $6CE
	; dc.w $46C
	; dc.w $24A
	; dc.w $AAA
	; dc.w $2CE
	; dc.w $666
	; dc.w $68E
	; dc.w $246
	; dc.w $222
	; dc.w $EEE
 Palette2:
    dc.w  0,$444,$888,$ECC,$2A0,$224,$222,$26,$8CE,$A,$EE
	dc.w $246,$268,$48C, 0,$CEE
 Palette3:
    dc.w  0,$444,$666,$AAA,$A4,$202,$604,$246,$880,$E2A,$222
	dc.w $268,$48C,$8CE, 0,$CEE
 Palette4:
    dc.w  0,$444,$666,$AAA,$A6E,$26,$4A,$8CE,$40,$28C,$4A0
	dc.w $246,$48C,$ACE, 2,$CEE
 Palette5:
    dc.w  0,$444,$666,$AAA,$E82,$628,$A2C,$E6E,$E20,$ECE,$EE
	dc.w $46,$46A,$6AE, 2,$ACE
	; dc.w $000
	; dc.w $444
	; dc.w $666
	; dc.w $AAA
	; dc.w $E82
	; dc.w $624
	; dc.w $C6A
	; dc.w $EAE
	; dc.w $E20
	; dc.w $ECE
	; dc.w $0EE
	; dc.w $226
	; dc.w $26A
	; dc.w $6AC
	; dc.w $000
	; dc.w $8CE
	
 Palette6:
    dc.w  0,$444,$666,$AAA, 4,$C,$222,$26,$6AC,$A00,$EE,$46
	dc.w $468,$6AC, 0,$AEE
 Palette7:
    dc.w  0,$444,$666,$AAA, 4,$68,$222,$26,$A,$A00,$EE,$46
	dc.w $468,$6AC, 0,$AEE
 Palette8:
    dc.w  0,$444,$666,$AAA, 4,$400,$222,$E80,$80,$A00,$EE
	dc.w $46,$268,$4AC, 0,$8CE
 Palette9:
    dc.w  0,$44,$288,$4EE, 4,$C,$22,$444,$888,$800,$840,$46
	dc.w $268,$6AC, 0,$AEE
 Palette10:
    dc.w  0,$444,$444,$888,$400,$C00,$222,$24,$888,$A00,$CE
	dc.w $46,$468,$6AC, 0,$AEE
 Palette11:
    dc.w  0,$40,$62,$A4,$4E,$A, 4, 2,$666,$EA0,$EE,$404,$888
	dc.w $CCC, 0,$EEE
 Palette12:
    dc.w  0,$444,$666,$AAA,$440,$224,$E40,$26,$ACC,$A,$EE
	dc.w $246,$468,$68A, 0,$EEE
 Palette13:
    dc.w  0,$444,$666,$AAA,$860,$224,$222,$26,$6AC,$A,$EE
	dc.w $246,$268,$48A, 0,$EEE
 Palette14:
    dc.w  0,$24,$246,$4AA,$240,$224,$222,$26,$6AC,$A,$EEE
	dc.w $246,$268,$48A, 0,$EEE
 Palette15:
    dc.w  0,$444,$666,$AAA, 4,$400,$222,$E80,$80,$A00,$EE
	dc.w $46,$68,$48C, 0,$6EE
 Palette16:
    dc.w  0,$444,$666,$C0,$60,$224, 0,$202,$604,$E06,$EEE
	dc.w $E,$268,$48A, 0,$EEE
 Palette17:
    dc.w  0,$444,$666,$AAA,$A00,$22, 2,$E80,$80,$24,$EE,$46
	dc.w $268,$4AC, 0,$8CE
 Palette18:
    dc.w  0,$444,$666, 8, 4,$224,$222,$400,$600,$E00,$EE,$C
	dc.w $268,$48A, 0,$EEE
 Palette19:
    dc.w  0,$444,$666,$E44,$622,$224,$222,$42,$62,$A2,$EE
	dc.w $C,$268,$48A, 0,$EEE
 Palette20:
    dc.w  0,$444,$666,$AAA,$24,$400,$222,$E80,$80,$A00,$EE
    dc.w $246,$468,$6AC, 0,$CEE
 ; Palette20: ;For Lang2Mod
	; dc.w $000
	; dc.w $242
	; dc.w $22C
	; dc.w $AEE
	; dc.w $6AC
	; dc.w $268
	; dc.w $246
	; dc.w $E84
	; dc.w $A64
	; dc.w $644
	; dc.w $A68
	; dc.w $2EE
	; dc.w $288
	; dc.w $846
	; dc.w $222
	; dc.w $EEE
 Palette21:
    dc.w  0,$444,$666,$AAA,$24,$40,$222,$A2,$80,$60,$EE,$246
	dc.w $468,$6AC, 0,$CEE
 Palette22:
    dc.w  0,$60,$40,$E80, 4,$68,$404,$26,$A,$A00,$AC,$46,$468
	dc.w $6AC, 0,$CEE
 Palette23:
    dc.w  0,$888,$442,$8E,$24,$EC0,$A40,$224,$C, 6,$466,$E0E
	dc.w $606,$800, 0,$AEE
 Palette24:
    dc.w  0,$444,$606,$E4A, 4,$400,$222,$E80,$68,$A00,$CE
    dc.w $244,$466,$6AA, 0,$AEE
 Palette25:
    dc.w  0,$444,$666,$AAA,$440,$224,$200,$26,$ACC,$A,$402
	dc.w $246,$468,$68A, 0,$EEE
 Palette26:
    dc.w  0,$444,$666,$AAA,$440,$224,$200,$26,$ACC,$A,$402
    dc.w $246,$468,$68A, 0,$EEE
 Palette27:
    dc.w  0,$444,$666,$E44,$622,$224, 0, 2, 6,$2A,$EE,$C,$46
    dc.w $6A, 0,$EEE
 Palette28:
    dc.w  0,$440,$862,$AAA,$42, 4,$200,$6A,$80,$28,$EE,$246
    dc.w $468,$6AC, 0,$CEE
 ; Palette28: ;For Lang2Mod
	; dc.w $000
	; dc.w $242
	; dc.w $22C
	; dc.w $AEE
	; dc.w $6AC
	; dc.w $268
	; dc.w $00C
	; dc.w $006
	; dc.w $004
	; dc.w $008
	; dc.w $A68
	; dc.w $2EE
	; dc.w $288
	; dc.w $046
	; dc.w $222
	; dc.w $EEE	
 Palette29:
    dc.w  0,$204,$206,$40A,$C0E,$A,$44,$24,$246, 2,$444,$666
	dc.w $888,$CCC, 0,$EEE
 Palette30:
    dc.w  0,$204,$206,$40A,$C0E,$A,$44,$24,$246, 2,$444,$666
    dc.w $888,$CCC, 0,$EEE
 Palette31:
    dc.w  0,$444,$666,$AAA,$24,$400,$222,$E80,$80,$A00,$EE
    dc.w $246,$468,$6AC, 0,$CEE
 Palette32:
    dc.w  0,$40,$62,$A4,$4E,$A, 4, 2,$666,$EA0,$EE,$404,$888
    dc.w $CCC, 0,$EEE
 Palette33:
    dc.w  0,$40,$62,$A4,$4E,$A, 4, 2,$666,$EA0,$EE,$404,$888
	dc.w $CCC, 0,$EEE
 Palette34:
    dc.w  0,$24,$46,$68,$46,$448,$244,$222,$64,$6A,$4A,$622
	dc.w $888,$CCC, 0,$88
 Palette35:
    dc.w  0,$240,$260,$4A0,$EA,$68,$46,$24,$440,$480,$EE,$20
	dc.w $888,$CCC, 0,$EE8
 Palette36:
    dc.w  0,$222,$444,$666,$CC0,$660,$440,$420,$666,$EA0,$880
    dc.w $402,$888,$CCC, 0,$EE6
 Palette37:
    dc.w $442,$444,$666,$AAA,$244,$224,$222,$400,$600,$E20
	dc.w $EE,$246,$268,$48A, 0,$EEE
 Palette38:
    dc.w $442,$444,$666,$AAA,$802,$224,$222,$42,$62,$A2,$EE
	dc.w $246,$268,$48A, 0,$EEE
 Palette39:
    dc.w $442,$204,$206,$40A,$C0E,$A,$44,$68,$EE,$422,$644
    dc.w $866,$A88,$CAA, 0,$EEE